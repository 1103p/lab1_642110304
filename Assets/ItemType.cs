using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ItemType : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}

namespace Supakorn.GameDev3.Chapter1
{
    public enum ItemType
    {
        COIN,
        BIGCOIN,
        POWERUP,
        POWERDOWN,
    }
}